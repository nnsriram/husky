#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/ximgproc/disparity_filter.hpp"
// #include <opencv2/contrib/contrib.hpp>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/OccupancyGrid.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/crop_box.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>



#include <stdio.h>
#include <string.h>

using namespace std;

pcl::PCLPointCloud2::Ptr cloud_data (new pcl::PCLPointCloud2 ());
pcl::PCLPointCloud2::Ptr cloud_filtered (new pcl::PCLPointCloud2 ());

// pcl::PCLPointCloud2::Ptr cloud_planes (new pcl::PCLPointCloud2 ());

sensor_msgs::PointCloud2::Ptr cloud_planes (new sensor_msgs::PointCloud2);
pcl::PointCloud<pcl::PointXYZ>::Ptr tmp_cloud_planes (new pcl::PointCloud<pcl::PointXYZ>);

ros::Publisher vox_pub;

void cloud_callback(sensor_msgs::PointCloud2 cloud_msg)
{
	pcl_conversions::toPCL(cloud_msg , *cloud_data);

	//------------- Voxel Grid--------------------------------//
	pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
	sor.setInputCloud (cloud_data);
	sor.setLeafSize (0.04f, 0.04f, 0.04f);
	sor.filter (*cloud_filtered);

	// vox_pub.publish(cloud_filtered);

	//------------------ PASS THROUGH--------------------------------//
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_pass (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_seg (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr not_plane_points (new pcl::PointCloud<pcl::PointXYZ>);

	pcl::fromPCLPointCloud2(*cloud_filtered, *cloud_seg);

	pcl::PassThrough<pcl::PointXYZ> pass;
	pass.setInputCloud (cloud_seg);
	pass.setFilterFieldName ("z");
	pass.setFilterLimits (0.0, 20.0);
	//pass.setFilterLimitsNegative (true);
	pass.filter (*cloud_pass);

	*cloud_seg = *cloud_pass;
	//------------------ RANSAC-----------------------------------//



	

	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
  	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

  	pcl::SACSegmentation<pcl::PointXYZ> seg;

  	seg.setOptimizeCoefficients (true);

  	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setDistanceThreshold (0.2);

	seg.setInputCloud (cloud_seg);
	seg.segment (*inliers, *coefficients);

	tmp_cloud_planes->header.frame_id = "camera";
	tmp_cloud_planes->points.clear();
	// for(int i = 0; i<inliers->indices.size(); i++)
	// {
	// 	tmp_cloud_planes->points.push_back(cloud_seg->points[inliers->indices[i]]);
	// }

 	pcl::ExtractIndices<pcl::PointXYZ> extract;
	extract.setInputCloud (cloud_seg);
	extract.setIndices (inliers);
	extract.setNegative (false);

	extract.filter(*tmp_cloud_planes);

	extract.setNegative (true);
    extract.filter (*not_plane_points);

    //----------------------- Euclidean Cluster Extraction -------------------//
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);

 	tree->setInputCloud (not_plane_points);

	std::vector<pcl::PointIndices> cluster_indices;
	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance (0.05); // 2cm
	ec.setMinClusterSize (80);
	ec.setMaxClusterSize (50000);
	ec.setSearchMethod (tree);
	ec.setInputCloud (not_plane_points);
	ec.extract (cluster_indices);

	cloud_cluster->is_dense = true;
	cloud_cluster->header.frame_id = "camera";

	int j = 0;
	for(std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
	{
		for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
		{
			cloud_cluster->points.push_back(not_plane_points->points[*pit]);

		}
		j++;
	}
	cout<<j<<'\n';
	pcl::toROSMsg(*cloud_cluster, *cloud_planes);

	vox_pub.publish(cloud_cluster);

}

void vox_calc()
{


	// vox_pub.publish(cloud_filtered);
	return;
}
int main(int argc, char** argv)
{
	ros::init(argc, argv, "obs_detect");
	ros::NodeHandle n;

	ros::Subscriber point_sub = n.subscribe("/disp_point_cloud",1,cloud_callback);

	vox_pub = n.advertise<pcl::PCLPointCloud2>("/vox_points",1);

	while(ros::ok())
	{
		// if(cloud_data->width > 0)
		// 	vox_calc();
		ros::spinOnce();
	}
return 0;

}