#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/OccupancyGrid.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/crop_box.h>

#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/Pose.h>
#include <math.h>

#define m_size 100
#define sf 5 // scale factor 

using namespace std;

nav_msgs::OccupancyGrid occ_map;
std::vector<float> points[m_size][m_size];
std::vector<signed char> v((m_size)*(m_size));

geometry_msgs::Pose pos;

ros::Publisher map_pub;


int mean[m_size][m_size]={};
float variance[m_size][m_size]={};

float computeMean(std::vector<float> in){
	float sum = 0;
	for(unsigned int i = 0; i < in.size(); i++)
		sum += in[i];
	return sum/in.size();
}

float computeVariance(std::vector<float> in, float mean){
	float sum = 0;
	for(unsigned int i = 0; i < in.size(); i++)
		sum += (in[i] - mean) * (in[i] - mean);
	return sqrt(sum/(in.size()));
}

void points_callback(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& msg)
{
	
	pcl::PointXYZRGB p;

	for(int i=0; i<m_size; i++)
		for(int j=0; j<m_size; j++)
			points[i][j].clear();
	
	int m,n;
	// cout<<msg->points[0].y<<'\n';

	for(int i=0; i<(msg->width*msg->height); i++)
	{
		p = msg->points[i];

		p.x = sf*p.x;
		p.y = sf*p.y;
		p.z = sf*p.z;

		// p.y = p.y + (m_size/2);
		m = p.z;
		n = p.x + (m_size/2); //+(m_size/2);

		if (m >=0 && m < 100)
			if(n >=0 && n < 100)
			{
				// if(p.x > 1)
				// 	cout<<p.x<<' ';
				points[m][n].push_back(p.y);
				// v[m*m_size + n] = 100;

			}
	}

	for(int i = 0; i < m_size; i++)
	{
		for(int j=0; j<m_size; j++)
		{
			// cout<<points[i][j].size()<<' ';
			if(points[i][j].size() == 0)
			{
				// v[i*m_size + j] = 50;
				continue;
			}
			mean[i][j] = computeMean(points[i][j]);
			variance[i][j] = computeVariance(points[i][j], mean[i][j]);
			if(fabs(variance[i][j]) > 0.0)
				v[i*m_size + j] = 0;

		}
	}
	// occ_map.info.resolution = 1;
	occ_map.info.width = m_size;
	occ_map.info.height = m_size;
	occ_map.header.frame_id = "/map";
	occ_map.header.stamp = ros::Time::now();
	pos.position.x = 0;
	pos.position.y = 0;
	pos.position.z = 0;
	occ_map.info.origin = pos;
	occ_map.info.origin.orientation.w = 1.0;

	
}

void orig_callback(const pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr& msg)
{
	pcl::PointXYZRGB p;

	int m,n;

	for( int i=0; i<m_size; i++)
		for(int j = 0; j<m_size; j++)
			v[i*m_size + j] = 50;

	for(int i=0; i<(msg->width*msg->height); i++)
	{
		p = msg->points[i];

		p.x = sf*p.x;
		p.y = sf*p.y;
		p.z = sf*p.z;

		// p.y = p.y + (m_size/2);
		m = p.z;
		n = p.x + (m_size/2); //+(m_size/2);

		if (m >=0 && m < 100)
			if(n >=0 && n < 100)
				v[m*m_size + n] = 100;
	}
}

int main(int argc, char** argv)
{
	
	ros::init(argc, argv, "matrix_node");
	ros::NodeHandle n;
	
	occ_map.info.resolution = 1;
	occ_map.info.width = m_size;
	occ_map.info.height = m_size;
	occ_map.header.frame_id = "/map";

	map_pub = n.advertise<nav_msgs::OccupancyGrid>("/costmap", 1);
	ros::Subscriber points_sub = n.subscribe("/vox_points",1,points_callback);
	ros::Subscriber disp_sub = n.subscribe("/disp_point_cloud", 1, orig_callback);


	while(ros::ok())
	{
		// cout<<occ_map.header.frame_id<<'\n';
		occ_map.data = v;
		map_pub.publish(occ_map);
		ros::spinOnce();
	}
	return 0;

}