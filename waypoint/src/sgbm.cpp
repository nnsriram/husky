#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/ximgproc/disparity_filter.hpp"
// #include <opencv2/contrib/contrib.hpp>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/OccupancyGrid.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/pcl_base.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/crop_box.h>

#include <stdio.h>
#include <string.h>

using namespace cv;
using namespace cv::ximgproc;
using namespace std;


cv_bridge::CvImagePtr left_img;
cv_bridge::CvImagePtr right_img;

Mat left_load,right_load;

pcl::PointCloud<pcl::PointXYZRGB> point_cloud_data;
ros::Publisher point_pub;

int minDisparity;
int numberOfDisparities;
int SADWindowSize;
int preFilterCap;
int uniquenessRatio;
int P1, P2;
int speckleWindowSize;
int speckleRange;
int disp12MaxDiff;
int block_size;

//------------ Post filtering ----------------//
double matching_time, filtering_time;

void calcDepth()
{
    point_cloud_data.header.frame_id = "camera";
    // point_cloud_data.height = 360;
    // point_cloud_data.width = 640;
    // point_cloud_data.header.stamp = ros::Time::now().toSec();

    Mat img1, img2;
    Mat disp, disp8;
    Mat g1, g2;
    char m_s[] = "SGBM" ;
    char* method = m_s;

    // --- enable for zed ----//
    // img1 = left_img->image;
    // img2 = right_img->image;

    // -----enable for kitti---------//
    img1 = left_load;
    img2 = right_load;
    // cout<<img1.size()<<'\n';

    // resize(img1, img1, Size(640,360));
    // resize(img2, img2, Size(640,360));

    cvtColor(img1, g1, CV_BGR2GRAY);
    cvtColor(img2, g2, CV_BGR2GRAY);

//------------- Disparity map post filtering-----------------//
    Mat left_disp,right_disp;
    Mat filtered_disp;
    cv::Ptr<cv::ximgproc::DisparityWLSFilter> wls_filter;

    // if (!(strcmp(method, "BM")))
    // {
    //     cv::Ptr<cv::StereoSGBM> sbm = cv::StereoSGBM::create();
    //     // StereoBM sbm;
    //     sbm->SADWindowSize = 9;
    //     sbm.state->numberOfDisparities = 112;
    //     sbm.state->preFilterSize = 5;
    //     sbm.state->preFilterCap = 61;
    //     sbm.state->minDisparity = -39;
    //     sbm.state->textureThreshold = 507;
    //     sbm.state->uniquenessRatio = 0;
    //     sbm.state->speckleWindowSize = 0;
    //     sbm.state->speckleRange = 8;
    //     sbm.state->disp12MaxDiff = 1;
    //     sbm(g1, g2, disp);
    // }
    if (!(strcmp(method, "SGBM")))
    {
        // StereoSGBM sbm;
        cv::Ptr<cv::StereoSGBM> sbm = cv::StereoSGBM::create();
        // sbm->setBlockSize(15);
        // sbm->setNumDisparities(112);
        // sbm->setPreFilterCap(15);
        // sbm->setMinDisparity(0);
        // sbm->setUniquenessRatio(0);
        // sbm->setSpeckleWindowSize(200);
        // sbm->setSpeckleRange(4);
        // sbm->setDisp12MaxDiff(0);
        // // sbm.fullDP = false;
        // sbm->setP1(50);
        // sbm->setP2(800);
        // sbm->compute(g1, g2, disp);

        sbm->setBlockSize(block_size);
        sbm->setNumDisparities(numberOfDisparities);
        sbm->setPreFilterCap(preFilterCap);
        sbm->setMinDisparity(minDisparity);
        sbm->setUniquenessRatio(uniquenessRatio);
        sbm->setSpeckleWindowSize(speckleWindowSize);
        sbm->setSpeckleRange(speckleRange);
        sbm->setDisp12MaxDiff(disp12MaxDiff);
        sbm->setP1(P1);
        sbm->setP2(P2);
        // sbm->compute(g1, g2, disp);

//------------ Post filtering--------------------------------//
        wls_filter = createDisparityWLSFilter(sbm);
        cv::Ptr<StereoMatcher> right_matcher = createRightMatcher(sbm);

        // matching_time = (double)getTickCount();
        sbm->compute(g1,g2,left_disp);
        right_matcher->compute(g2,g1,right_disp);
        // matching_time = ((double)getTickCount() - matching_time)/getTickFrequency();
    }

    // cout<<"Done sgbm \n";

    wls_filter->setLambda(8000);
    wls_filter->setSigmaColor(1.5);
    // filtering_time = (double)getTickCount();
    wls_filter->filter(left_disp,img1,filtered_disp,right_disp);
    // filtering_time = ((double)getTickCount() - filtering_time)/getTickFrequency();

    // cout.precision(2);
    // cout<<matching_time<<' '<<filtering_time<<'\n';
    
    disp = filtered_disp;
    normalize(disp, disp8, 0, 255, CV_MINMAX, CV_8U);

    // imshow("left", img1);
    // imshow("right", img2);
    imshow("disp", disp8);


    waitKey(10);
    
    int height = img1.size().height;
    int width = img1.size().width;
    float  mn = 1000,mx = 0,tmp;
    Mat depth(height, width, CV_32FC1, 0.0);
    int count = 0;
    //pcl::PointXYZ min_t;
    //pcl::PointXYZ max_t;
    //min_t.x = 10000; min_t.y = 10000; min_t.z = 10000;
    //max_t.x = -10000; max_t.y = -10000; max_t.z = -10000;

    // ---------- ZED PARAMS---------------------//
    // double cx = 349.15;//659.976 * 0.5;//671.541 * 0.5; //K[0][2];
    // double cy = 188.551;//391.648 * 0.5;// 364.044 * 0.5; //K[1][2];
    // double f = 349.449;//* 0.5;//699.022 *0.5;//K[0][0];
    // double baselength = 0.12001;   // in meters


    // ---------------KITTI PARAMS----------------//
    double cx = 609.5593;//659.976 * 0.5;//671.541 * 0.5; //K[0][2];
    double cy = 172.8540;//391.648 * 0.5;// 364.044 * 0.5; //K[1][2];
    double f = 721.5377;//* 0.5;//699.022 *0.5;//K[0][0];
    double baselength = 0.47055642414;   // in meters
    /*
       Mat points;
       Mat Q = (Mat_<double>(4,4) << 1, 0, 0, -cx, 0, 1, 0, -cy, 0, 0, 0, f, 0, 0, -1/(baselength), 0);
       reprojectImageTo3D(disp, points, Q, true);
     */
    for(int i=0; i<height; i++)
    {
        const short *Mi = disp.ptr<short>(i);
        const Vec3b *Ci = img1.ptr<Vec3b>(i);
        float *Di = depth.ptr<float>(i);
        //const Point3f *Qi = depth.ptr<Point3f>(i);

        for(int j=0; j< width; j++)
        {

            pcl::PointXYZRGB point;

            uint8_t cb = Ci[j].val[0];
            uint8_t cg = Ci[j].val[1];
            uint8_t cr = Ci[j].val[2];

            uint32_t rgb = ((uint32_t)cr << 16 | (uint32_t)cg << 8 | (uint32_t)cb);
            point.rgb = *reinterpret_cast<float*>(&rgb);
            //point.rgb = rgb;

            //cv::Point2f cameraCenter;

            float Z = Mi[j] /16.0f; // Check 16.0f

            if(Z > 0.0 ){
                //double D = (f*baselength)/static_cast<double>(Z);

                mx = max(mx, Z);
                mn = min(mn, Z);
                double pw =  static_cast<double>(Z) / baselength;

                double X = (static_cast<double>(j) - cx) / pw;
                double Y = (static_cast<double>(i) - cy) / pw;
                double D = f / pw;

                //float X = (j - cameraCenter.x) * Di[j] * (1/K[0][0]);
                //float Y = (i - cameraCenter.y) * Di[j] * (1/K[1][1]);
                point.z = D ;
                point.x = X;
                point.y = Y;

                /*
                   point.z = Qi[j].z;
                   point.x = Qi[j].x;
                   point.y = Qi[j].y;
                 */
                Di[j] = D;
                //point.rgb = cr << 16 | cg << 8 | cb;
                //if(Di[j] < 100){
                point_cloud_data.points.push_back (point);          
            }

            }
        }
        point_pub.publish(point_cloud_data.makeShared());
        point_cloud_data.points.clear();
}

void left_img_cb(const sensor_msgs::ImageConstPtr& msg)
{
    // cv_bridge::CvImagePtr left_img;

    try
    {
    left_img =  cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
    }

    // cout<<left_img->encoding;

    // imshow("left", left_img->image);
    // waitKey(1);

  // Process cv_ptr->image using OpenCV
}

void right_img_cb(const sensor_msgs::ImageConstPtr& msg)
{
    try
    {
    right_img = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
    }

    // imshow("right", right_img->image);
    // waitKey(1);
  // Process cv_ptr->image using OpenCV
}

static void on_trackbar(int, void*)
{
return;
}
int main(int argc, char* argv[])
{
    ros::init(argc,argv,"depth_converter");
    ros::NodeHandle n;
    // ros::NodeHandle private_nh("~");

    image_transport::ImageTransport it(n);

    // cv::namedWindow("left");
    // cv::namedWindow("right");
    cv::namedWindow("disp");

    // ----------- ZED ------------//
    // minDisparity = 0;
    // numberOfDisparities = 112;
    // // SADWindowSize;
    // preFilterCap = 15;
    // uniquenessRatio;
    // P1 = 50;
    // P2 = 800;
    // speckleWindowSize = 200;
    // speckleRange = 4;
    // disp12MaxDiff = 0;

    // -------- KITTI------------//
    minDisparity = 0;
    numberOfDisparities = 112;
    // SADWindowSize;
    block_size = 3;
    preFilterCap = 63;
    uniquenessRatio = 10;
    P1 = block_size*block_size*8;
    P2 = block_size*block_size*32;
    speckleWindowSize = 100;
    speckleRange = 32;
    disp12MaxDiff = 1;

    cv::createTrackbar("PreFilterCap","disp",&preFilterCap, 63 ,on_trackbar); 
    // cv::createTrackbar("numberOfDisparities","disp",&numberOfDisparities, 256 ,on_trackbar); 
    cv::createTrackbar("minDisparity","disp",&minDisparity, 128 ,on_trackbar); 
    cv::createTrackbar("uniquenessRatio","disp",&uniquenessRatio, 100 ,on_trackbar);   
    cv::createTrackbar("speckleWindowSize","disp",&speckleWindowSize, 1000 ,on_trackbar); 
    cv::createTrackbar("speckleRange","disp",&speckleRange, 32 ,on_trackbar); 
    cv::createTrackbar("disp12MaxDiff","disp",&disp12MaxDiff, 128 ,on_trackbar); 
    cv::createTrackbar("P1","disp",&P1, 4000 ,on_trackbar); 
    cv::createTrackbar("P2","disp",&P2, 4000 ,on_trackbar); 


    // image_transport::Subscriber image_left = it.subscribe("/zed/left/image_rect_color", 1, left_img_cb);
    // image_transport::Subscriber image_right = it.subscribe("/zed/right/image_rect_color", 1, right_img_cb);

    point_pub  = n.advertise<sensor_msgs::PointCloud2>("/disp_point_cloud",1);

    
    // image_transport::Publisher image_pub;
    // ros::Duration(2).sleep();
    cv::VideoCapture cap_left("/home/husky/data_scene_flow/training/image_2/%06d_10.png");
    cv::VideoCapture cap_right("/home/husky/data_scene_flow/training/image_3/%06d_10.png");
    while(ros::ok())
    {
        // left_load = cv::imread("/home/husky/data_scene_flow/training/image_2/000002_10.png",CV_LOAD_IMAGE_COLOR);
        // right_load = cv::imread("/home/husky/data_scene_flow/training/image_3/000002_10.png",CV_LOAD_IMAGE_COLOR);
        cap_left >> left_load;
        cap_right >> right_load;

        imshow("left", left_load);
        // imshow("right", right_load);

        if(left_load.data)
        {
            calcDepth();
        }
        int k = waitKey(0) & 0xff;
        if(k == 27)
            continue;
        if(left_img && right_img)
            calcDepth();
        ros::spinOnce();
    }

    ros::spin();

   
    return(0);
}