from numpy import *


def midline(startp, endp, pts):
	m = (endp[1] - startp[1])/(endp[0] - startp[0])
	xx = zeros([1,pts])
	yy = zeros([1,pts])
	if isinf(m) == True:
		xx[0:pts-1]=startp[0];
		yy[0:pts-1]=linspace(startp[1],endp[1],pts);
	elif m==0 : #%horizontal line
		xx[0:pts-1]=linspace(startp[0],endp[0],pts);
		yy[0:pts-1]=startp[1];
	else: #%if (endp(1)-startp(1))~=0
		xx=linspace(startp[0],endp[0],pts);
		yy=m*(xx-startp[0])+startp[1];	
	return xx, yy