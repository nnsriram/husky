from numpy import *
import matplotlib.pyplot as plt
import math
from scipy.special import comb
from midline import midline
import sys
from scipy.interpolate import interp1d


x=[0]
y=[0]

file = open("../../../waypoints.txt","r")

for line in file:
    value = line.split()
    if value == []:
        break
    x.append(float(value[0]))
    y.append(float(value[1]))
# plt.axis([-20, 20, -20, 20])
# x=[0, 0, 0	]
# y=[0, 10, 15]

c=0;kappa=[];PX=zeros([200]);PY=zeros([200]);thet=zeros([599]);arclength=zeros([599]);kappa=zeros([598])
kappad=[]
px=zeros([len(x)-2,200])
py=zeros([len(x)-2,200])
mem_x = [x[0]];fmemx=[];
mem_y = [y[0]];fmemy=[];
while c<(len(x) -2 ) :
	MW1=[x[c], y[c], 0];
	MW2=[x[c+1], y[c+1], 0];
	MW3=[x[c+2], y[c+2], 0];
	WW1 = subtract(MW2,MW1);
	WW2 = subtract(MW3,MW2);
	cross_val = cross(WW1,WW2);
	cross_mag = sqrt(cross_val[0]**2 + cross_val[1]**2 + cross_val[2]**2);
	ww1_mag = sqrt(WW1[0]**2 + WW1[1]**2);
	ww2_mag = sqrt(WW2[0]**2 + WW2[1]**2);
	gamma = math.asin(cross_mag/(ww1_mag*ww2_mag));

	# setting initial parameters based on maximum curvature begins here
	c2=((sqrt(6)-1)*2)/5;
	c1=7.2364;
	c3=(c2+4)/(c1+6);
	Kmax= 100;       #%%parameter of maximum curvature
	beta=(pi-gamma)/2;
	if(gamma == 0) :
		beta = 0;
	d=((c2+4)**2 * sin(beta))/(54*c3*Kmax*cos(beta)*cos(beta));
	Hb=c3*d;
	He=c3*d;
	gb=c2*c3*d;
	ge=c2*c3*d;
	kb=(6*c3*cos(beta)*d)/(c2+4);
	ke=kb;
	# parameter setting ends here
	W1=vstack((x[c],y[c]));
	W2=vstack((x[c+1] ,y[c+1]));
	W3=vstack((x[c+2] ,y[c+2]));
	W2W1=sqrt((x[c+1]-x[c])**2 + (y[c+1]-y[c])**2);
	u1=(1/W2W1)*vstack((x[c]-x[c+1],y[c]-y[c+1]));
	W2W3=sqrt((x[c+1]-x[c+2])**2 + (y[c+1]-y[c+2])**2);
	u2=(1/W2W3)*vstack((x[c+2]-x[c+1],y[c+2]-y[c+1]));
	B0=W2+(d*u1);
	B1=B0-(gb*u1);
	B2=B1-(Hb*u1);
	mem_x = vstack((mem_x,B0[0]))
	mem_y = vstack((mem_y,B0[1]));


	E0=W2+(d*u2);
	E1=E0-(ge*u2);
	E2=E1-(He*u2);
	mem_x = vstack((mem_x,E0[0]));
	mem_y =vstack((mem_y,E0[1]))
	# plot([x(c+2);E0(1)],[y(c+2);E0(2)],'b','Linewidth',1.5);
	# plot([B2(1),E2(1)],[B2(2),E2(2)],'k')
	# plot(B2(1),B2(2),'kO')

	B2E2=sqrt((E2[1]-B2[1])**2 + (E2[0]-B2[0])**2);
	print B2E2
	ud=(1/B2E2)*[E2.item(0)-B2.item(0),E2.item(1)-B2.item(1)];
	B3=B2+(kb*ud);
	B3=B3[:,0]
	E3=E2-(ke*ud);
	E3=E3[:,0] 
	  # plot(E3(1),E3(2),'bO')

	# plot(B3,'bO')
	# plot(E3,'rO')
	# B0 B1 B2 B3 E3 E2 E1 E0 are the control points to generate two beizer curve  the two beizer curve are found to be curvature continuous at the point of intersection

	dt=0.01;
	t=0;i=0;
	X1=[B0[0], B1[0], B2[0], B3[0], E3[0], E2[0], E1[0], E0[0]];
	Y1=[B0[1], B1[1], B2[1], B3[1], E3[1], E2[1], E1[1], E0[1]];
	X=[B0[0], B1[0], B2[0], B3[0]];#% control points for first beizer curve
	Y=[B0[1], B1[1], B2[1], B3[1]];
	while(t<=1):
		Be0=comb(3,0)*(t**0)*((1-t)**3); #%bernstein coeff
		Be1=comb(3,1)*(t**1)*((1-t)**2);
		Be2=comb(3,2)*(t**2)*((1-t)**1);
		Be3=comb(3,3)*(t**3)*((1-t)**0);
		PX[i]=(X[0]*Be0)+(X[1]*Be1)+(X[2]*Be2)+(X[3]*Be3); #% x equation of beizer curve
		PY[i]=(Y[0]*Be0)+(Y[1]*Be1)+(Y[2]*Be2)+(Y[3]*Be3); #% y equation of beizer curve
		i=i+1;t=t+dt;

	t=0;
	X=[E3[0], E2[0], E1[0], E0[0]];  #% control points for second beizer curve
	Y=[E3[1], E2[1], E1[1], E0[1]];
	while(t<=1):
		Be0=comb(3,0)*(t**0)*((1-t)**3); #%bernstein coeff
		Be1=comb(3,1)*(t**1)*((1-t)**2);
		Be2=comb(3,2)*(t**2)*((1-t)**1);
		Be3=comb(3,3)*(t**3)*((1-t)**0);
		PX[i]=(X[0]*Be0)+(X[1]*Be1)+(X[2]*Be2)+(X[3]*Be3); #% x equation of beizer curve
		PY[i]=(Y[0]*Be0)+(Y[1]*Be1)+(Y[2]*Be2)+(Y[3]*Be3); #% y equation of beizer curve
		i=i+1;t=t+dt;

	px[c,:] = PX;py[c,:] = PY;
	# find curvature and d(curvature)/dt of the first beizer curve 
	x0=B0[0];x1=B1[0];x2=B2[0];x3=B3[0];y0=B0[1];y1=B1[1];y2=B2[1];y3=B3[1];
	# [curvaturen1] = findc(x0,x1,x2,x3,y0,y1,y2,y3,dt); 
	# kappa=horzcat(kappa,curvaturen1);

	#  find curvature and d(curvature)/dt of the second beizer curve
	x0=E3[0];x1=E2[0];x2=E1[0];x3=E0[0];y0=E3[1];y1=E2[1];y2=E1[1];y3=E0[1];
	# [curvaturen2] = findc(x0,x1,x2,x3,y0,y1,y2,y3,dt);
	# % kappa=horzcat(kappa,curvaturen2);

	# % find curvature and d(curvature)/ds
	# % kappa1=horzcat(curvaturen1,curvaturen2);
	# % i=1;
	# %  while(i<(max(size(PX))-2))
	# %     ds=sqrt(((PX(i+1)-PX(i))^2) + (PY(i+1)-PY(i))^2);
	# %     dkds(i)=(kappa1(i+1)-kappa1(i))/ds;
	# %     i=i+1;
	# %  end
	# % kappad=horzcat(kappad,dkds);
	c=c+1;
mem_x = vstack((mem_x,x[-1]));
mem_y = vstack((mem_y,y[-1]));
mem = hstack((mem_x,mem_y))
print mem
wayx  = zeros([len(mem)/2, 200]);
wayy  = zeros([len(mem)/2, 200]);
kit  = 0;
j = 0 ;
while (kit < size(mem,0) - 1) :
	if mod(kit,2) == 0 :
		wayx[j,:],wayy[j,:]  = midline(mem[kit,:],mem[kit+1,:],200);
		j = j +1;
	kit = kit +1 ;

j = 0; k = 0;
fx = [];fy=[];
while(j <=size(wayx,0)-1):
	if j == 0 :
		fx = transpose(wayx[j,:]);
		fy = transpose(wayy[j,:]);
		j = j+1;
	else :
		if isnan(px[k,0]) == True :
			fx = hstack((fx,transpose(wayx[j,:])));
			fy = hstack((fy,transpose(wayy[j,:])));
			j = j+1;k=k+1;
		else :
			fx = hstack((fx,transpose(px[k,:])))
			fx = hstack((fx,transpose(wayx[j,:])))    
			fy = hstack((fy,transpose(py[k,:])))
			fy = hstack((fy,transpose(wayy[j,:])))    
			j = j+1;k=k+1;
final = vstack((fx,fy));
# final2 = hstack((fx,fy))
final = transpose(final)
plt.plot(final[:,0],final[:,1]);
plt.show()
print(len(final))
arr = array(final)
savetxt("../../../points.txt",arr,fmt="%10.5f")
