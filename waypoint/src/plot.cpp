#include <ros/ros.h>
#include <ros/package.h>
#include <fstream>
#include <utility>
#include <vector>
// #include <move_base_msgs/MoveBaseAction.h>
// #include <actionlib/client/simple_action_client.h>
#include <robot_localization/navsat_conversions.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Bool.h>
#include <tf/transform_listener.h>
#include <math.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>

double x_pos,y_pos,theta;
std::ofstream out_file;

void pose_callback(nav_msgs::Odometry odom_msg)
{
		y_pos = odom_msg.pose.pose.position.y;
		x_pos = odom_msg.pose.pose.position.x;
		theta = odom_msg.pose.pose.orientation.z;
		out_file<<x_pos<<' '<<y_pos<<'\n';
}

int main(int argc, char** argv)
{

	ros::init(argc,argv,"plot");
	
	ros::NodeHandle n;
	ros::NodeHandle private_nh("~");
	ros::TransportHints noDelay = ros::TransportHints().tcpNoDelay(true);

	out_file.open("points.txt");

	ros::Subscriber pose_sub = n.subscribe("/odometry/filtered",1,pose_callback);
	while(ros::ok())
	{
		ros::spinOnce();
	}
	return 0;
}