#include "ros/ros.h"						
#include "geometry_msgs/Twist.h"			
#include "geometry_msgs/Vector3.h"			
#include "geometry_msgs/Pose.h"	
#include "std_msgs/Empty.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#include "tf/transform_datatypes.h"

#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Dense>


#define set_loop_rate 100


ros::Publisher velocity_pub;

sensor_msgs::Imu accn_msg;
nav_msgs::Odometry pose_msg;
nav_msgs::Odometry temp_pose_msg[50];
geometry_msgs::Twist velocity_msg;

void imu_callback(const sensor_msgs::Imu::ConstPtr& imu_msg)
{
	accn_msg.header = imu_msg->header; 												/////header.seq
	accn_msg.angular_velocity = imu_msg->angular_velocity;							/////angular_velocity.x
	accn_msg.linear_acceleration = imu_msg->linear_acceleration;						/////linear_acceleration.x	

}

void odom_callback(const nav_msgs::Odometry::ConstPtr& odom_msg)
{
	static int odom_cb = 0;
	
	pose_msg.header = odom_msg->header;													/////header.seq
	pose_msg.pose.pose.position = odom_msg->pose.pose.position;							/////pose.pose.position.x
	pose_msg.pose.pose.orientation = odom_msg->pose.pose.orientation;					/////pose.pose.orientation.x   /////quaternion	
	pose_msg.twist.twist.linear = odom_msg->twist.twist.linear;						    /////twist.twist.linear.x
	pose_msg.twist.twist.angular = odom_msg->twist.twist.angular;						/////twist.twist.angular.x

	if(odom_cb>0)
	ROS_INFO("time = %lf",(pose_msg.header.stamp.sec+pose_msg.header.stamp.nsec*pow(10,-9)) - (temp_pose_msg[odom_cb-1].header.stamp.sec+temp_pose_msg[odom_cb-1].header.stamp.nsec*pow(10,-9)));					
	
	temp_pose_msg[odom_cb] = pose_msg;
	//temp_dt[odom_cb] =  (pose_msg.header.stamp.sec+pose_msg.header.stamp.nsec*pow(10,-9)) - (temp_pose_msg[odom_cb].header.stamp.sec+temp_pose_msg[odom_cb].header.stamp.nsec*pow(10,-9));

	if(odom_cb==49)
	{	
		//double temp_x=0, temp_y=0, temp_t=0;

		pose_msg.twist.twist.linear.x = (pose_msg.pose.pose.position.x - temp_pose_msg[0].pose.pose.position.x)/(0.02*50); //((pose_msg.header.stamp.sec+pose_msg.header.stamp.nsec*pow(10,-9)) - (temp_pose_msg[odom_cb-9].header.stamp.sec+temp_pose_msg[odom_cb-9].header.stamp.nsec*pow(10,-9)));

		ROS_INFO("velocity_x = %f", pose_msg.twist.twist.linear.x );
	
		std::rotate(temp_pose_msg, temp_pose_msg+1, temp_pose_msg+50);
		odom_cb=48;
	}

	odom_cb++;
	


 	// ROS_INFO("Seq: [%d]", odom_msg->header.seq);
 	// ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", odom_msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
 	// ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", odom_msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
 	// ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", odom_msg->twist.twist.linear.x,msg->twist.twist.angular.z);

}

void publish_vw()
{
	velocity_msg.linear.x = 0;
	velocity_msg.linear.y = 0;
	velocity_msg.linear.z = 0;

	velocity_msg.angular.x = 0;
	velocity_msg.angular.y = 0;
	velocity_msg.angular.z = 0;

	velocity_pub.publish(velocity_msg);
}


int main(int argc, char **argv)
{
  
  	ros::init(argc, argv, "get_data");

  	ros::NodeHandle n;

	ros::Subscriber odom_sub = n.subscribe("/odometry/filtered", 1000, odom_callback);
	ros::Subscriber imu_sub = n.subscribe("/imu/data", 1000, imu_callback);


  	velocity_pub = n.advertise<geometry_msgs::Twist>("/husky_velocity_controller/cmd_vel", 1000);

  	ros::Rate loop_rate(set_loop_rate);


  	while (ros::ok())
  	{
    	
    	
 	  	 	
  		



  		publish_vw();

    	ros::spinOnce();
    	loop_rate.sleep();
    }


  	return 0;

}
