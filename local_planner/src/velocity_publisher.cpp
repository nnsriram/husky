#include "ros/ros.h"
#include "ros/message.h"	
#include "geometry_msgs/Twist.h"			
#include "local_planner/planned_velocity.h"


#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Eigen>
#include <Eigen/Dense>
#define PI 3.14159265

#define set_publisher_rate 50												// ros loop_rate



using namespace std;

ros::Publisher velocity_pub;

geometry_msgs::Twist velocity_msg[1000];


int vel_mesg_counter=0;


void velocity_callback(const local_planner::planned_velocity::ConstPtr& vel_msg)
{	
	static int temp_instant=0;

	if(temp_instant = !vel_msg->planning_instant_no)
		vel_mesg_counter=0;

	if((vel_msg->stop_publishing)==0)
	{
		//velocity_msg[vel_mesg_counter].header = vel_msg->header; 						
		velocity_msg[vel_mesg_counter].angular = vel_msg->twist.angular;							
		velocity_msg[vel_mesg_counter].linear = vel_msg->twist.linear;						
		vel_mesg_counter++;
	}
	
	temp_instant = vel_msg->planning_instant_no;

}



// void publish_vw(int x)
// {	

// 	//cout<<x<<endl;
// 	//if((x < (sizeof(global_velocity)/sizeof(global_velocity[0]))) && (!stop_replanning))
// 	if(x<0)
// 	{
// 		velocity_msg.linear.x = (double)max_robot_velocity;
// 		velocity_msg.linear.y = 0;
// 		velocity_msg.linear.z = 0;
				
// 		velocity_msg.angular.x = 0;
// 		velocity_msg.angular.y = 0;
// 		velocity_msg.angular.z = 0;
// 		printf("velocity = %lf \n",velocity_msg.linear.x);
// 	}	
	
// 	else
// 	{	
// 		if(!stop_replanning)
// 		{	
		
// 			// double query_time = x/(double)set_loop_rate;			
// 			// double query_index = query_time/exec_t_step;                   /////////// (x/loop_rate) will be between 2 timesteps of index*exec_t_step
// 			// double index_time_1 = floor(query_index)*exec_t_step;
// 			// double index_time_2 = ceil(query_index)*exec_t_step;

// 			// cout<<"query_time="<<query_time<<" query_index="<<query_index<<" index_time1="<<index_time_1<<" index_time2="<<index_time_2<<endl;
// 			// cout<<"global_velocity1="<<global_velocity[(int)floor(query_index)]<<" global_velocity2="<<global_velocity[(int)ceil(query_index)]<<endl;
// 			// cout<<"global_omega1="<<global_omega[(int)floor(query_index)]<<" global_omega2="<<global_omega[(int)ceil(query_index)]<<endl;
			

// 			// double final_velocity = ((index_time_2 - query_time)*global_velocity[(int)floor(query_index)] + (query_time-index_time_1)*global_velocity[(int)ceil(query_index)])/(index_time_2 - index_time_1) ;
// 			// double final_omega = ((index_time_2 - query_time)*global_omega[(int)floor(query_index)] + (query_time-index_time_1)*global_omega[(int)ceil(query_index)])/(index_time_2 - index_time_1) ;

			
// 			double query_index = (x/(double)set_loop_rate)/exec_t_step;                   /////////// (x/loop_rate) will be between 2 timesteps of index*exec_t_step
			
// 			double final_velocity = ((ceil(query_index)-query_index)*global_velocity[(int)floor(query_index)] + (query_index-floor(query_index))*global_velocity[(int)ceil(query_index)])/(ceil(query_index) - floor(query_index)) ;
// 			double final_omega = ((ceil(query_index)-query_index)*global_omega[(int)floor(query_index)] + (query_index-floor(query_index))*global_omega[(int)ceil(query_index)])/(ceil(query_index) - floor(query_index)) ;
			


// 			velocity_msg.linear.x = final_velocity;
// 			velocity_msg.linear.y = 0;
// 			velocity_msg.linear.z = 0;
				
// 			velocity_msg.angular.x = 0;
// 			velocity_msg.angular.y = 0;
// 			velocity_msg.angular.z = final_omega;

// 			//prev_global_theta_x = global_theta_x[x];

// 			//printf("z = %d velocity = %lf , omega = %lf, global_theta_x = %lf \n", x,velocity_msg.linear.x, velocity_msg.angular.z, global_theta_x[x]);
// 			velocity_pub.publish(velocity_msg);	

// 			if(ceil(query_index)>=cl_pt_indices)
// 			{	
// 				stop_replanning=1;
// 				cout<<"stopped planning"<<endl;
// 			}	

// 		}	
		
// 		else
// 		{
// 			velocity_msg.linear.x = 0;
// 			velocity_msg.linear.y = 0;
// 			velocity_msg.linear.z = 0;

// 			velocity_msg.angular.x = 0;
// 			velocity_msg.angular.y = 0;
// 			velocity_msg.angular.z = 0;

// 			velocity_pub.publish(velocity_msg);	

// 			if(stop_replanning || last_plan)
// 				ros::shutdown();
// 		}	
// 	}
// }



int main(int argc, char **argv)
{
  
  	ros::init(argc, argv, "local_planner_velocity_publisher");

  	ros::NodeHandle nh;

	ros::Subscriber vel_sub = nh.subscribe("/planned_velocity", 1000, velocity_callback);
	
  	velocity_pub = nh.advertise<geometry_msgs::Twist>("/husky_velocity_controller/cmd_vel", 0);
  	
  	ros::Rate loop_rate(set_publisher_rate);

  	int z=0;

  	while (ros::ok())
  	{
    	
  		//publish_vw(z+1);
  		z++;
  		//ROS_INFO("z = %d",z);
	
    	ros::spinOnce();
    	loop_rate.sleep();
    }

  	return 0;

}
