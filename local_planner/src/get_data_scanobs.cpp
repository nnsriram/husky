#include "ros/ros.h"						
#include "geometry_msgs/Twist.h"			
#include "geometry_msgs/Vector3.h"			
#include "geometry_msgs/Pose.h"	
#include "std_msgs/Empty.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#include "nav_msgs/OccupancyGrid.h"
#include "sensor_msgs/LaserScan.h"
#include "tf/transform_datatypes.h"
#include "tf/transform_listener.h"

#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Dense>


#define set_loop_rate 100


ros::Publisher velocity_pub;

sensor_msgs::Imu accn_msg;
nav_msgs::Odometry pose_msg;
geometry_msgs::Twist velocity_msg;
sensor_msgs::LaserScan obs_msg;




int obs_no;

float obstacle_local_distance[10], obstacle_local_angle[10],obstacle_radius[10];

void imu_callback(const sensor_msgs::Imu::ConstPtr& imu_msg)
{
	accn_msg.header = imu_msg->header; 												/////header.seq
	accn_msg.angular_velocity = imu_msg->angular_velocity;							/////angular_velocity.x
	accn_msg.linear_acceleration = imu_msg->linear_acceleration;						/////linear_acceleration.x	

}

void odom_callback(const nav_msgs::Odometry::ConstPtr& odom_msg)
{

	pose_msg.header = odom_msg->header;													/////header.seq
	pose_msg.pose.pose.position = odom_msg->pose.pose.position;							/////pose.pose.position.x
	pose_msg.pose.pose.orientation = odom_msg->pose.pose.orientation;					/////pose.pose.orientation.x   /////quaternion	
	pose_msg.twist.twist.linear = odom_msg->twist.twist.linear;							/////twist.twist.linear.x	
	pose_msg.twist.twist.angular = odom_msg->twist.twist.angular;						/////twist.twist.angular.x
	

 	// ROS_INFO("Seq: [%d]", odom_msg->header.seq);
 	// ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", odom_msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
 	// ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", odom_msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
 	// ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", odom_msg->twist.twist.linear.x,msg->twist.twist.angular.z);

}


void scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan_msg)
{
	obs_msg.header = scan_msg->header;
	obs_msg.angle_min = scan_msg->angle_min;
	obs_msg.angle_max = scan_msg->angle_max;
	obs_msg.angle_increment = scan_msg->angle_increment;
	obs_msg.range_min = scan_msg->range_min;
	obs_msg.range_max = scan_msg->range_max;	
	obs_msg.ranges = scan_msg->ranges;

	obs_no=-1;
	float i=obs_msg.angle_min,temp_distance=0,temp_angle=0,delta_angle=0,prev_angle=0;
	int j=0,prev_j=0,count=0;



	while(i<obs_msg.angle_max)
	{
		if((obs_msg.ranges[j] >= obs_msg.range_min) && (obs_msg.ranges[j] <= obs_msg.range_max))
		{
			
			if(abs(j-prev_j) > 50)
			{

				obs_no++;
				//ROS_INFO("obs_no = %d",obs_no);
				prev_j = j;

				if(obs_no>0)
				{
					//ROS_INFO("current-angle %lf",i);	
					obstacle_local_distance[obs_no-1] = temp_distance/count; 
					obstacle_local_angle[obs_no-1] = (-temp_angle)/count;
					obstacle_radius[obs_no-1] = (obstacle_local_distance[obs_no-1]*(delta_angle)/2);
					ROS_INFO("obs(%d) local_distance = %lf local_angle = %lf radius = %lf", obs_no-1,obstacle_local_distance[obs_no-1], obstacle_local_angle[obs_no-1], obstacle_radius[obs_no-1]);
				}

				temp_distance=0;
				temp_angle=0;
				count=0;
				prev_angle = i;
				//ROS_INFO("initial angle %lf",delta_angle);
			}	

			temp_distance += obs_msg.ranges[j];
			temp_angle += i;
			count++;
			delta_angle = i-prev_angle;
		}		

		j++;
		i+=obs_msg.angle_increment;
	}

	obstacle_local_distance[obs_no] = temp_distance/count; 
	obstacle_local_angle[obs_no] = (-temp_angle)/count;
	obstacle_radius[obs_no] = (obstacle_local_distance[obs_no]*(delta_angle)/2);
	ROS_INFO("obs(%d) local_distance = %lf local_angle = %lf radius = %lf", obs_no,obstacle_local_distance[obs_no], obstacle_local_angle[obs_no],obstacle_radius[obs_no]);
	

	ROS_INFO("end");
}



void publish_vw()
{
	velocity_msg.linear.x = 0;
	velocity_msg.linear.y = 0;
	velocity_msg.linear.z = 0;

	velocity_msg.angular.x = 0;
	velocity_msg.angular.y = 0;
	velocity_msg.angular.z = 0;

	velocity_pub.publish(velocity_msg);
}


int main(int argc, char **argv)
{
  
  	ros::init(argc, argv, "get_data");

  	ros::NodeHandle n;

	ros::Subscriber odom_sub = n.subscribe("/odometry/filtered", 1000, odom_callback);
	ros::Subscriber imu_sub = n.subscribe("/imu/data", 1000, imu_callback);
	ros::Subscriber scan_sub = n.subscribe("/scan", 1000, scan_callback );

  	velocity_pub = n.advertise<geometry_msgs::Twist>("/husky_velocity_controller/cmd_vel", 1000);


  	tf::TransformListener listener;

  	ros::Rate loop_rate(set_loop_rate);


  	while (ros::ok())
  	{
    	
    	tf::StampedTransform transform;
        
        try
        {
      		listener.lookupTransform("base_link", "odom", ros::Time(0), transform);
    	}
   		catch (tf::TransformException &ex) 
   		{
      		ROS_ERROR("%s",ex.what());
      		ros::Duration(1.0).sleep();
      		continue;
    	}
		
		geometry_msgs::PoseStamped obs_pose_msg;

		obs_pose_msg.pose.position.x = transform.getOrigin().x();
		obs_pose_msg.pose.position.y = transform.getOrigin().y();
		obs_pose_msg.pose.orientation.z = transform.getRotation().z();
		obs_pose_msg.pose.orientation.w = transform.getRotation().w();
		obs_pose_msg.header.stamp = ros::Time(0);
		obs_pose_msg.header.frame_id = "init_link";

		ROS_INFO("x= %lf y=%lf",obs_pose_msg.pose.position.x,obs_pose_msg.pose.position.y );
 	  	 	
  		



  		//publish_vw();

    	ros::spinOnce();
    	loop_rate.sleep();
    }


  	return 0;

}
