#include "ros/ros.h"						
#include "geometry_msgs/Twist.h"			
#include "geometry_msgs/Vector3.h"			
#include "geometry_msgs/Pose.h"	
#include "std_msgs/Empty.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/Imu.h"
#include "sensor_msgs/LaserScan.h"
#include "nav_msgs/Odometry.h"
#include "tf/transform_datatypes.h"
#include "ros/callback_queue.h"


#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Dense>
#define PI 3.14159265


#define max_robot_curvature 0.4            								// curvature=1/turning_radius*****
#define max_robot_velocity 1               								// robot=husky
#define road_width 6.5

#define robot_length 1
#define robot_width 0.7
#define robot_radius 0.6
#define max_deviation ((road_width/2)-robot_length)                    	// according to road_width and robot_width, the max deviation from the center line possible for the robot


#define cl_dt 0.01								  						// for t=0 to 1 in bezier curve we want 100 points per section (200=2bezier curve)
#define cl_way_pt_no 3                                       			// change this when changing no of waypoints
#define	cl_traj_pt ((cl_way_pt_no -2)*2/cl_dt + (cl_way_pt_no -1)*200)

#define vs1_var 10 
#define d1_var 20          												// change for number of trajectories

#define traj_time_div 200												// each traj divided in 200 small time scaled divisions	


#define set_loop_rate 10												// ros loop_rate



using Eigen::MatrixXd;
using Eigen::VectorXd;
using namespace Eigen;
using namespace std;



///////////////////ros variables///////////////////
ros::Publisher velocity_pub;
sensor_msgs::Imu accn_msg;
nav_msgs::Odometry pose_msg;
geometry_msgs::Twist velocity_msg;
sensor_msgs::LaserScan obs_msg;


/////////////////obstacle variables////////////////////
int obs_no;
float obstacle_local_distance[10], obstacle_local_angle[10],obstacle_radius[10];



/////////////traj_parameter global variables//////////////////////
double t0=0,t1, s0=0,s1,vs0=0,vs1,as0=0,as1, d0=0,d1,vd0=0,vd1,ad0=0,ad1;              ////starting states for every replanning stage   


double robot_current_x,robot_current_y,robot_current_yaw;      			 /////from odometry
	


////////////////center_line global variables/////////////////
Vector2d cl_way_pt[cl_way_pt_no]; 													/// center line waypoints		
Vector2d cl_way_traj[(int)cl_traj_pt];		/// center line trajectory (x,y)	
double theta_r[(int)(cl_traj_pt)], arc_length[(int)(cl_traj_pt)], kappa[(int)(cl_traj_pt)];



//////////////////final_optimum_traj global variables/////////////// 
int optimum_traj_no;
double global_velocity[(int)cl_traj_pt], global_omega[(int)cl_traj_pt], global_theta_x[(int)cl_traj_pt], global_x[(int)cl_traj_pt], global_y[(int)cl_traj_pt]; 

int prev_cl_pt=0;
bool stop_replanning=0;


class obstacle
{
	//// obs_center_s, obs_center_d, clearing_radius
	
	public:
		static int obs_counter;
		
		double center_s,center_d,clearing_radius;
		
		obstacle(){
			obs_counter++;
		}
}; 
int obstacle::obs_counter = 0;                                      ////////////////use (obs_no) not this
obstacle obs[100];

class sd_traj
{
	//// matrix st_mat,ds_mat,a,b,cs_mat,cd_mat, &  collision_status, cost, max_curvature_status,
	public:
		static int traj_counter;
		
		double t0, t1;  //s0,vs0,as0,  d0,vd0,ad0,  t1,  s1,vs1,as1,  d1,vd1,ad1;
		MatrixXd a_mat, b_mat, c_a_mat, c_b_mat  ;
		
		double cost,max_curvature_value; 
		bool max_curvature_status,collision_status;

		double st[200], ds[200];
		
		sd_traj() : a_mat(6,1), b_mat(6,1), c_a_mat(6,1), c_b_mat(6,1) {						///constructor
			traj_counter++;
		}              
		
};
int sd_traj::traj_counter = 0;
sd_traj traj[vs1_var*d1_var];									//////define objects for trajectories





void imu_callback(const sensor_msgs::Imu::ConstPtr& imu_msg)
{
	accn_msg.header = imu_msg->header; 												/////header.seq
	accn_msg.angular_velocity = imu_msg->angular_velocity;							/////angular_velocity.x
	accn_msg.linear_acceleration = imu_msg->linear_acceleration;						/////linear_acceleration.x	
	//ROS_INFO("imu_updated");
}

void odom_callback(const nav_msgs::Odometry::ConstPtr& odom_msg)
{

	pose_msg.header = odom_msg->header;													/////header.seq
	pose_msg.pose.pose.position = odom_msg->pose.pose.position;							/////pose.pose.position.x
	pose_msg.pose.pose.orientation = odom_msg->pose.pose.orientation;					/////pose.pose.orientation.x   /////quaternion	
	pose_msg.twist.twist.linear = odom_msg->twist.twist.linear;							/////twist.twist.linear.x	
	pose_msg.twist.twist.angular = odom_msg->twist.twist.angular;						/////twist.twist.angular.x
	
	//ROS_INFO("odom_updated");	

 	// ROS_INFO("Seq: [%d]", odom_msg->header.seq);
 	// ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", odom_msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
 	// ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", odom_msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
 	// ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", odom_msg->twist.twist.linear.x,msg->twist.twist.angular.z);

}


void scan_callback(const sensor_msgs::LaserScan::ConstPtr& scan_msg)
{
	obs_msg.header = scan_msg->header;
	obs_msg.angle_min = scan_msg->angle_min;
	obs_msg.angle_max = scan_msg->angle_max;
	obs_msg.angle_increment = scan_msg->angle_increment;
	obs_msg.range_min = scan_msg->range_min;
	//obs_msg.range_max = scan_msg->range_max;
	obs_msg.range_max = 10;																		//////////////////////////////////////////////
	obs_msg.ranges = scan_msg->ranges;

	obs_no=-1;
	float i=obs_msg.angle_min,temp_distance=0,temp_angle=0,delta_angle=0,prev_angle=0;
	int j=0,prev_j=0,count=0;



	while(i<obs_msg.angle_max)
	{
		if((obs_msg.ranges[j] >= obs_msg.range_min) && (obs_msg.ranges[j] <= obs_msg.range_max))
		{
			
			if(abs(j-prev_j) > 50)
			{

				obs_no++;
				//ROS_INFO("obs_no = %d",obs_no);
				prev_j = j;

				if(obs_no>0)
				{
					//ROS_INFO("current-angle %lf",i);	
					obstacle_local_distance[obs_no-1] = temp_distance/count; 
					obstacle_local_angle[obs_no-1] = temp_angle/count;
					obstacle_radius[obs_no-1] = (obstacle_local_distance[obs_no-1]*(delta_angle)/2);
					//ROS_INFO("obs(%d) local_distance = %lf local_angle = %lf radius = %lf", obs_no-1,obstacle_local_distance[obs_no-1], obstacle_local_angle[obs_no-1], obstacle_radius[obs_no-1]);
				}

				temp_distance=0;
				temp_angle=0;
				count=0;
				prev_angle = i;
				//ROS_INFO("initial angle %lf",delta_angle);
			}	

			temp_distance += obs_msg.ranges[j];
			temp_angle += i;
			count++;
			delta_angle = i-prev_angle;
		}		

		j++;
		i+=obs_msg.angle_increment;
	}

	obstacle_local_distance[obs_no] = temp_distance/count; 
	obstacle_local_angle[obs_no] = temp_angle/count;
	obstacle_radius[obs_no] = (obstacle_local_distance[obs_no]*(delta_angle)/2);
	//ROS_INFO("obs(%d) local_distance = %lf local_angle = %lf radius = %lf", obs_no,obstacle_local_distance[obs_no], obstacle_local_angle[obs_no],obstacle_radius[obs_no]);
	

	//ROS_INFO("end");


}


void publish_vw(int x)
{	
	int i = x;


	if((x < (sizeof(global_velocity)/sizeof(global_velocity[0])))) //&& (!stop_replanning))
	{	
					
		velocity_msg.linear.x = global_velocity[i];
		velocity_msg.linear.y = 0;
		velocity_msg.linear.z = 0;
			
		velocity_msg.angular.x = 0;
		velocity_msg.angular.y = 0;
		velocity_msg.angular.z = global_omega[i];

		printf("z = %d velocity = %lf , omega = %lf, global_theta_x = %lf \n", i,global_velocity[i], global_omega[i], global_theta_x[i]);
		velocity_pub.publish(velocity_msg);	
	}	
	
	else
	{
		velocity_msg.linear.x = 0;
		velocity_msg.linear.y = 0;
		velocity_msg.linear.z = 0;

		velocity_msg.angular.x = 0;
		velocity_msg.angular.y = 0;
		velocity_msg.angular.z = 0;

		velocity_pub.publish(velocity_msg);	
	}	
	
}



void get_global_waypoints()
{	
	cl_way_pt[0] <<  0, 0;
	cl_way_pt[1] <<  40, 0;												//////0,0; 36,0; 36,-45; 
	cl_way_pt[2] <<  40, -45;
	// cl_way_pt[3] <<  10, 30;
	// cl_way_pt[4] <<  40, 30;
	// cl_way_pt[5] <<  60, 10;

	
}

void get_center_line()
{
	//// 2 bezier curve (for continous dkappa/ds) with linear interpolation in between the sets of curves to get the complete center_line trajectory

	ofstream myfile;
  	myfile.open ("center_line_coordinates.txt"); 			

	Vector2d W1, W2, W3, U1, U2, Ud, B0, B1, B2, B3, E0, E1, E2, E3;	

	double c1=7.2364, c2=(sqrt(6)-1)*2/5, c3, kmax=max_robot_curvature;  
	double beta, gammaa, dk, hb, he, gb, ge, kb, ke ;

	int ways=cl_way_pt_no,traj_pt=0;

	c3 = (c2+4)/(c1+6);

	int i=0;
	double temp_slope1,temp_slope2;
	double t=0;

	cl_way_traj[traj_pt] << 0,0;
	Vector2d temp_pos = cl_way_traj[traj_pt], temp_pos1;
	traj_pt++;
		
	

	while(i < (ways-2))
	{	


		// W[0][0]= way_x[i]; W[0][1]= way_y[i]; 
		// W[1][0]= way_x[i+1]; W[1][1]= way_y[i+1];
		// W[2][0]= way_x[i+2]; W[2][1]= way_y[i+2];	
		
		// temp_slope1 = atan((cl_way_pt[i+1](1)-cl_way_pt[i](1))/(cl_way_pt[i+1](0)-cl_way_pt[i](0))); 	
		// temp_slope2 = atan((cl_way_pt[i+2](1)-cl_way_pt[i+1](1))/(cl_way_pt[i+2](0)-cl_way_pt[i+1](0))); 	


		W1 = cl_way_pt[i];
		W2 = cl_way_pt[i+1];
		W3 = cl_way_pt[i+2];

		// W1 = cl_way_pt[1];
		// W2 = cl_way_pt[2];
		// W3 = cl_way_pt[3];
		// //cout<<" W1 " << W1 <<" W2 "<<W2 <<" W3 "<< W3<<endl; 

		temp_slope1 = atan((W2(1) - W1(1))/(W2(0) - W1(0)));	
		temp_slope2 = atan((W3(1) - W2(1))/(W3(0) - W2(0)));
		//cout << temp_slope1 <<" "<< temp_slope1<<endl;	

		if (temp_slope1<0)
			temp_slope1=temp_slope1+PI;

		if (temp_slope2<0)
			temp_slope2=temp_slope2+PI;
		
		
		
		gammaa = std::abs(temp_slope2 - temp_slope1);
		beta = gammaa/2;
		dk = (pow(c2+4,2)*sin(beta))/(54*c3*kmax*pow(cos(beta),2));
		hb=he=c3*dk; 
		gb=ge=c2*c3*dk;
		kb=ke=(6*c3*cos(beta)*dk)/(c2+4);	

		//U1 = (W[1]-W[2])/(W[1].norm() * W[2].norm());
		//U2 = (W[2]-W[3])/(W[2].norm() * W[3].norm());

		U1 = (W1-W2)/((W1-W2).norm());
		U2 = (W3-W2)/((W3-W2).norm());
		
		B0 = W2 + dk*U1;
		B1 = B0 - gb*U1;
		B2 = B1 - hb*U1;
	
		E0 = W2 + dk*U2;
		E1 = E0 - ge*U2;
		E2 = E1 - he*U2;

		Ud = (E2-B2)/((E2-B2).norm());

		B3 = B2 + kb*Ud;
		E3 = E2 - ke*Ud;


		// cout << "Here is the vector B0:\n" << B[0] << endl;	
		// cout << "Here is the vector B1:\n" << B[1] << endl;	
		// cout << "Here is the vector B2:\n" << B[2] << endl;



		////interpolation code 

		temp_pos1 = (pow(1-t,3)*B0 + 3*pow(1-t,2)*t*B1 + 3*(1-t)*pow(t,2)*B2 + pow(t,3)*B3);
		//cout<<"temp_pos1 "<<temp_pos1<<endl;

		//if(traj_pt==1)
		{	
		
			if(( temp_pos1 - temp_pos ).norm() > 0.2)
			{
				Vector2d temp_step = (temp_pos1 - temp_pos)/200;
				for(int x=0; x<200; x++)
				{
					cl_way_traj[traj_pt] = cl_way_traj[traj_pt-1] + temp_step;
					//cout <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl;
					myfile <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl; 
					traj_pt++;	
				}

			}
		}
		////////////////////
			
		while(t<=1)
		{	
			cl_way_traj[traj_pt] = (pow(1-t,3)*B0 + 3*pow(1-t,2)*t*B1 + 3*(1-t)*pow(t,2)*B2 + pow(t,3)*B3); 	
			//cout <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl;
			myfile <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl;			
			traj_pt++;
			t += cl_dt;
		}	
		
		while(t>=0)
		{	
			cl_way_traj[traj_pt] = (pow(1-t,3)*E0 + 3*pow(1-t,2)*t*E1 + 3*(1-t)*pow(t,2)*E2 + pow(t,3)*E3); 	
			//cout <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl;
			myfile <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl;
			traj_pt++;
			t -= cl_dt;
		}


		temp_pos =  cl_way_traj[traj_pt-1];
		//cout << "temp_pos " <<temp_pos<<endl;
		i++;
	}


		////interpolation code 

		temp_pos1 = cl_way_pt[cl_way_pt_no-1];
		//cout<<"temp_pos1 "<<temp_pos1<<endl;

		//if(traj_pt==1)
		{	
		
			if(( temp_pos1 - temp_pos ).norm() > 0.2)
			{
				Vector2d temp_step = (temp_pos1 - temp_pos)/200;
				for(int x=0; x<200; x++)
				{
					cl_way_traj[traj_pt] = cl_way_traj[traj_pt-1] + temp_step;
					//cout <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl;
					myfile <<cl_way_traj[traj_pt](0)<<" "<<cl_way_traj[traj_pt](1)<< endl; 
					traj_pt++;	
				}

			}
		}
		////////////////////

	//////////////////////calculation for theta and kappa (curvature)	

	theta_r[0] = atan((cl_way_traj[1](1) - cl_way_traj[0](1))/(cl_way_traj[1](0) - cl_way_traj[0](0)));
	arc_length[0] = (cl_way_traj[1] - cl_way_traj[0]).norm();
	
	for(int j=1; j<cl_traj_pt-1; j++)
	{
		theta_r[j] = atan((cl_way_traj[j+1](1) - cl_way_traj[j](1))/(cl_way_traj[j+1](0) - cl_way_traj[j](0)));
		arc_length[j] = arc_length[j-1] + (cl_way_traj[j+1] - cl_way_traj[j]).norm();
		kappa[j-1] = (theta_r[j] - theta_r[j-1])/(arc_length[j] - arc_length[j-1]);			
		
		// cout<<"theta_r(" <<(j) << ") = " << theta_r[j]<<endl;
		// cout<<"arc_length(" <<(j) << ") = " << arc_length[j]<<endl;
		// cout<<"kappa(" <<(j-1) << ") = " << kappa[j-1]<< "\n"<<endl; 
	
	}
	
	theta_r[(int)cl_traj_pt-1] = theta_r[(int)cl_traj_pt-2];
	arc_length[(int)cl_traj_pt-1] = arc_length[(int)cl_traj_pt-2];
	kappa[(int)cl_traj_pt-1] = kappa[(int)cl_traj_pt-2] = kappa[(int)cl_traj_pt-3];
	
	// cout<<"theta_r(" <<(int)cl_traj_pt-1 << ") = " << theta_r[(int)cl_traj_pt-1]<<endl;
	// cout<<"arc_length(" <<(int)cl_traj_pt-1 << ") = " << arc_length[(int)cl_traj_pt-1]<<endl;
	// cout<<"kappa(" <<(int)cl_traj_pt-1 << ") = " << kappa[(int)cl_traj_pt-1]<< "\n"<<endl; 			

	/////////////////////////////////////
			
	myfile.close();

}


void get_current_statedata()
{
/// collect data from ROS; output: t0,  s0,vs0,as0,   d0,vd0,ad0	
	
	//extern obstacle obs[obs_no];
	static int count=0;
	count++;

	double s_dot,s_dot_dot, d_dot, d_dot_dot;    /////from IMU
	double d_dash, d_dash_dash;					
	double robot_current_s,robot_current_d;

	tf::Quaternion q(pose_msg.pose.pose.orientation.x, pose_msg.pose.pose.orientation.y, pose_msg.pose.pose.orientation.z, pose_msg.pose.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);
	//std::cout << "Roll: " << roll << ", Pitch: " << pitch << ", Yaw: " << yaw << std::endl;
	robot_current_yaw = yaw;


	robot_current_x = pose_msg.pose.pose.position.x;
	robot_current_y = pose_msg.pose.pose.position.y;
	

	//prev_cl_pt = 0;
	Vector2d robot_current_pos;
	robot_current_pos << robot_current_x,robot_current_y;  

	double temp_distance, lowest_distance = 1000, arc_length=0;
 	int closest_pt_index;

	for(int i=0; i<cl_traj_pt; i++)
	{
		
		temp_distance = (robot_current_pos - cl_way_traj[i]).norm();
		if(temp_distance < lowest_distance)
		{
			lowest_distance = temp_distance;
			closest_pt_index = i;
		}
			
	}
	
	for(int j=1; j<closest_pt_index+1; j++)
	{
		arc_length = arc_length + (cl_way_traj[j] - cl_way_traj[j-1]).norm();

	}

	prev_cl_pt = closest_pt_index;
	robot_current_s = arc_length;
	robot_current_d = lowest_distance;

	// if(prev_cl_pt==cl_traj_pt-1)
	// {
	// 	stop_replanning = 1;
	// }

	printf("robot current s = %lf d = %lf and last center_line point = %d \n",robot_current_s,robot_current_d,prev_cl_pt);

	s_dot = (sqrt(pow(pose_msg.twist.twist.linear.x,2) + pow(pose_msg.twist.twist.linear.y,2))) * cos(theta_r[prev_cl_pt] - robot_current_yaw);  
	d_dot = (sqrt(pow(pose_msg.twist.twist.linear.x,2) + pow(pose_msg.twist.twist.linear.y,2))) * sin(theta_r[prev_cl_pt] - robot_current_yaw); 

	s_dot_dot = (sqrt(pow(accn_msg.linear_acceleration.x,2) + pow(accn_msg.linear_acceleration.y,2))) * cos(theta_r[prev_cl_pt] - robot_current_yaw); 
	d_dot_dot = (sqrt(pow(accn_msg.linear_acceleration.x,2) + pow(accn_msg.linear_acceleration.y,2))) * sin(theta_r[prev_cl_pt] - robot_current_yaw); 

	printf("d_dot=%lf  d_dot_dot=%lf \n",d_dot,d_dot_dot);

	d_dash = s_dot/(-d_dot);
	d_dash_dash = ((-d_dot_dot) - d_dash*s_dot_dot)/(pow(s_dot,2));

	if(count==1)
	{	
		//ROS_INFO("jabddbakdsakjd");
		t0 = 0;					//////////////get system_time /clock
		s0 = 0;
		vs0 = 0;
		as0 = 0;	
		d0 = 0;
		vd0 = 0; 
		ad0 = 0;
	}
	
	else
	{	
		///for replanning update all of them with latest values
		t0 = 0;					//////////////get system_time /clock
		s0 = 0;
		vs0 = s_dot;
		as0 = s_dot_dot;	
		d0 = robot_current_d;
		vd0 = -d_dot;//d_dash; 
		ad0 = -d_dot_dot;//d_dash_dash;
		printf("current state data: vs0=%lf as0=%lf d0=%lf vd0=%lf ad0=%lf \n",vs0,as0,d0,vd0,ad0);
	}

}


void get_traj()
{
// input: current_statedata, max_deviation, max_velocity varying sets of d1,vs1   & t1,s1(optional for more sets & optimality) 
//output: sets of sd_traj 
	
	ofstream myfile1;
	myfile1.open ("frenet_frame_traj_1.txt");
 	
 	MatrixXd st_mat(6,6),c_st_mat(6,1),  ds_mat(6,6),c_ds_mat(6,1);
 	int traj_no = 0; 			
			

	
	as1 = 0;
	vd1 = 0;
	ad1 = 0;	
	t1 = t0 + 10;
	s1 = s0 + 10;
	
	st_mat << pow(t0,5),    pow(t0,4),    pow(t0,3),   pow(t0,2),   pow(t0,1), 1, 
			  pow(t1,5),    pow(t1,4),    pow(t1,3),   pow(t1,2),   pow(t1,1), 1,				
			  5*pow(t0,4),  4*pow(t0,3),  3*pow(t0,2), 2*pow(t0,1), 1,         0,
			  5*pow(t1,4),  4*pow(t1,3),  3*pow(t1,2), 2*pow(t1,1), 1,         0,
			  20*pow(t0,3), 12*pow(t0,2), 6*pow(t0,1), 2          , 0,         0,
			  20*pow(t1,3), 12*pow(t1,2), 6*pow(t1,1), 2          , 0,         0;

	ds_mat << pow(s0,5),    pow(s0,4),    pow(s0,3),   pow(s0,2),   pow(s0,1), 1, 
			  pow(s1,5),    pow(s1,4),    pow(s1,3),   pow(s1,2),   pow(s1,1), 1,				
			  5*pow(s0,4),  4*pow(s0,3),  3*pow(s0,2), 2*pow(s0,1), 1,         0,
			  5*pow(s1,4),  4*pow(s1,3),  3*pow(s1,2), 2*pow(s1,1), 1,         0,
			  20*pow(s0,3), 12*pow(s0,2), 6*pow(s0,1), 2          , 0,         0,
			  20*pow(s1,3), 12*pow(s1,2), 6*pow(s1,1), 2          , 0,         0;

	// int vs1_var=10, d1_var=10;          												//////change for number of trajectories
	// int traj_no=vs1_var*d1_var;
	// sd_traj traj[traj_no];

	double temp_vs1=0, temp_d1=-(double)max_deviation, temp_t1, temp_s1;		  
	double vs1_step = (double)max_robot_velocity/vs1_var;
	double d1_step = (double)max_deviation/(d1_var/2);		

	//cout<<"deviation_step " <<d1_step<<endl;

	for(int i=0; i<vs1_var; i++)
	{
		temp_vs1 =  temp_vs1 + vs1_step;

		for(int j=0; j<d1_var; j++)
		{	

			temp_d1 = temp_d1 + d1_step;

			c_st_mat << s0,
						s1,  
						vs0,
						temp_vs1,
						as0,
						as1;
			
			c_ds_mat << d0,
						temp_d1,  
						vd0,
						vd1,
						ad0,
						ad1;

									
			traj[traj_no].a_mat = st_mat.colPivHouseholderQr().solve(c_st_mat);			//colPivHouseholderQr()  llt()
			traj[traj_no].b_mat = ds_mat.colPivHouseholderQr().solve(c_ds_mat);						
			traj[traj_no].c_a_mat = c_st_mat;
			traj[traj_no].c_b_mat = c_ds_mat;
			traj[traj_no].t0 = t0;
			traj[traj_no].t1 = t1;

			//cout << "\nst:\n" << st_mat << endl;
			//cout << "\ncs:\n" << c_st_mat << endl;
			//cout << "\na:\n" << traj[traj_no].a_mat.transpose() << endl;
			//cout << "\nds:\n" << ds_mat << endl;
			//cout << "\ncd:\n" << c_ds_mat << endl;
			//cout << "\nb:\n" << traj[traj_no].b_mat.transpose() << endl;						
			
			//// cout << traj[traj_no].a_mat(0,0) <<	endl;
			//cout <<traj_no<<" "<<traj[traj_no].c_b_mat(1,0) << endl;

			myfile1 <<"\n"<< traj[traj_no].a_mat.transpose()<<endl ;
 			myfile1 <<"\n"<< traj[traj_no].c_a_mat.transpose()<<endl ;
			myfile1 <<"\n"<< traj[traj_no].b_mat.transpose()<<endl ;
			myfile1 <<"\n"<< traj[traj_no].c_b_mat.transpose()<<endl ;
			

			traj_no++;
		}

		temp_d1=(-(double)max_deviation);	
		//break;
	}
	
	myfile1.close();
	//cout<<sd_traj::traj_counter<<endl;
}


void get_obstacles()
{
//input: laser_data output: get obstacles from local laser frame to sd frame and their radius
//sample laser_data for non_zero values and get length and width of that obstacle
//then get center of obs in sdframe and clearing_radius (hypotenuse of lenth and width is diameter + husky_radius(0.6+0.2mclearance)) 
//closest point from center line(x,y) is s(t) and the deviation d(s) is perpendicular distance from that point 
//take only obstacles inside 10m distance 


	Vector2d obs_global_center[obs_no];
	
	for(int x=0; x<obs_no+1; x++)
	{
	
	double temp_x = robot_current_x + obstacle_local_distance[x]*cos(obstacle_local_angle[x]);						//local to global transformation
	double temp_y = robot_current_y + obstacle_local_distance[x]*sin(obstacle_local_angle[x]); 	
	obs_global_center[x] << temp_x, temp_y;

	double temp_distance, lowest_distance = 1000, arc_length=0;
 	int closest_pt_index;

	for(int i=prev_cl_pt; i<cl_traj_pt; i++)
	{
		
		temp_distance = (obs_global_center[x] - cl_way_traj[i]).norm();
		if(temp_distance < lowest_distance)
		{
			lowest_distance = temp_distance;
			closest_pt_index = i;
		}
			
	}
	
	for(int j=prev_cl_pt+1; j<closest_pt_index+1; j++)
	{
		arc_length = arc_length + (cl_way_traj[j] - cl_way_traj[j-1]).norm();

	}
	
	obs[x].center_d = lowest_distance;
	obs[x].center_s = arc_length;
	obs[x].clearing_radius = obstacle_radius[x] + (double)robot_radius + 0.2;  ///  safety 0.2
	 
	cout << "obstacle s,d = " << obs[x].center_s <<"," <<obs[x].center_d	<<" radius = " << obs[x].clearing_radius<< " and the closest point on center line = "<< cl_way_traj[closest_pt_index](0) << ","<< cl_way_traj[closest_pt_index](1)<<endl; 
	}


}

void check_collision()
{
/// one by one take all points on the sd_traj and compute distances from all available objects & simultaneously check if it is more than clearing_radius 
/// give collision_status 0(no collision) & 1(collision_detected)

	//ofstream myfile2;
	//myfile2.open ("traj_x_y_1.txt");


	for(int i=0; i< sd_traj::traj_counter ;i++)
	{
		double temp_T=0, temp_D=0;
		double t_step = (traj[i].t1 - traj[i].t0)/traj_time_div;

		for(int i1=0; i1<traj_time_div; i1++)
		{
			
			traj[i].st[i1] = traj[i].a_mat(0,0)*pow(temp_T,5) + traj[i].a_mat(1,0)*pow(temp_T,4) + traj[i].a_mat(2,0)*pow(temp_T,3) + traj[i].a_mat(3,0)*pow(temp_T,2) + 
								traj[i].a_mat(4,0)*temp_T + traj[i].a_mat(5,0);
			///// s(T)
								
			traj[i].ds[i1] = traj[i].b_mat(0,0)*pow(traj[i].st[i1],5) + traj[i].b_mat(1,0)*pow(traj[i].st[i1],4) + traj[i].b_mat(2,0)*pow(traj[i].st[i1],3) + traj[i].b_mat(3,0)*pow(traj[i].st[i1],2) + 
								traj[i].b_mat(4,0)*traj[i].st[i1] + traj[i].b_mat(5,0);

			////// d(s(T))

			//if(i==0)					
			// {
			// 	myfile2 <<""<< traj[i].ds[i1]<<" "<< traj[i].st[i1]<<endl ;
 		// 	}

			
			for(int j=0; j<obs_no+1; j++)
			{

				temp_D = pow(std::abs(obs[j].center_s - traj[i].st[i1]) ,2) + pow(std::abs(obs[j].center_d - traj[i].ds[i1]) ,2);      //// D^2 > rc^2 for every obstacle on the trajpoint selected   

				//cout<<"distance = "<<sqrt(temp_D) << endl;
				//cout<< obs[j].clearing_radius <<" radius"<<	endl;

				//double temp_adu = (sqrt(temp_D)) - (obs[j].clearing_radius);
				//static int adu_counter=0;
				//cout<<temp<<endl;

			

				if((sqrt(temp_D)) - (obs[j].clearing_radius)<=0)
				{
					//adu_counter++;	
					traj[i].collision_status = 1;
					//cout <<i<< " = collision and its deviation = " << traj[i].c_b_mat(1,0) <<endl;
				}
				else
				{
					traj[i].collision_status = 0;
					//cout<<"not collided "<< i <<endl;
					
				}

			}
			
			if(traj[i].collision_status == 1)
				break;	

			temp_T = temp_T + t_step;

		}	

	}

	///////////print collision_status of all trajectory
	// for(int i=0; i<sd_traj::traj_counter; i++)
	// {cout << "collision of traj(" <<i<< ") = " << traj[i].collision_status << " and its deviation = " << traj[i].c_b_mat(1,0)<< endl;}

//	myfile2.close();

}


bool sort_low_cost (sd_traj& traj_a, sd_traj& traj_b)
{
	return (traj_a.cost < traj_b.cost);
}


void compute_cost()
{
///calculate cost for collision_status-0 and simultaneously sort sd_traj ascendingly 
	
	
	//double kj_lon=0.001, kt_lon=1, ks_lon=0, kj_lat=0.0001, ks_lat=0.0001, kd_lat=20, k_lon=1, k_lat=1;							// tune heuristics
	double kj_lon=0.001, kt_lon=0, ks_lon=0, kj_lat=0.0001, ks_lat=0.0000, kd_lat=20, k_lon=1, k_lat=1;							// tune heuristics


	for(int i=0; i< sd_traj::traj_counter ;i++)
	{
		
		double cost_lon=0, cost_lat=0;

		if(traj[i].collision_status == 0)
		{
			double del_t = traj[i].t1 - traj[i].t0 , del_s = traj[i].c_a_mat(1,0)-s0;	    				//////del_t=t1-t0, del_s=s1-s0 of that particular traj

			double cost_jerklon = 720*(pow(traj[i].a_mat(0,0),2)*pow(del_t,5)) + (720*(traj[i].a_mat(0,0)*traj[i].a_mat(1,0))*pow(del_t,4)) + 
								(240*(traj[i].a_mat(0,0)*traj[i].a_mat(2,0)) + 192*(pow(traj[i].a_mat(1,0),2)))*pow(del_t,3) + (144*(traj[i].a_mat(1,0)*traj[i].a_mat(2,0))*pow(del_t,2))+ + (36*(pow(traj[i].a_mat(2,0),2)*del_t));

			cost_lon = kj_lon*cost_jerklon + kt_lon*del_t + ks_lon*pow(del_s,2);

			double cost_jerklat = 720*(pow(traj[i].b_mat(0,0),2)*pow(del_s,5)) + (720*(traj[i].b_mat(0,0)*traj[i].b_mat(1,0))*pow(del_s,4)) + 
								(240*(traj[i].b_mat(0,0)*traj[i].b_mat(2,0)) + 192*(pow(traj[i].b_mat(1,0),2)))*pow(del_s,3) + (144*(traj[i].b_mat(1,0)*traj[i].b_mat(2,0))*pow(del_s,2))+ + (36*(pow(traj[i].b_mat(2,0),2)*del_s));

			cost_lat = kj_lat*cost_jerklat + ks_lat*del_s + kd_lat*pow((traj[i].c_b_mat(1,0)),2);
			
			traj[i].cost = k_lat* cost_lat + k_lon* cost_lon;
			
			//cout << "cost of traj(" <<i<< ") = " << traj[i].cost << " and its deviation = " << traj[i].c_b_mat(1,0) << endl;  
		}

		else
		{
			traj[i].cost = 1000000;

		}

	}

	//std::sort(traj, traj+sd_traj::traj_counter, [] (sd_traj& traj_a, sd_traj& traj_b) -> bool { return (traj_a.cost < traj_b.cost); });    

	std::sort(traj, traj+sd_traj::traj_counter, sort_low_cost);									////sort all trajectories according to minimum cost

	///////////print low_cost_sorted trajectory
	// for(int i=0; i<sd_traj::traj_counter; i++)
	// {cout << "cost of traj(" <<i<< ") = " << traj[i].cost << " and its deviation = " << traj[i].c_b_mat(1,0) << endl;  }

}



void check_curvature_convert2vw()
{
////start from lowest-cost traj and calculate Kmax and check till condition is satisfied
	
	ofstream myfile3;
	myfile3.open ("final_traj.txt");

	
	///////////////////////kappa_max and velocity omega

	int cl_pt_no;

	for(int i=0; i< sd_traj::traj_counter ;i++)
	{	
		cl_pt_no =0;
		double temp_arclength = traj[i].c_a_mat(0,0) ; 
		while(temp_arclength < traj[i].c_a_mat(1,0))
		{
			temp_arclength = temp_arclength + (arc_length[prev_cl_pt + cl_pt_no+1]-arc_length[prev_cl_pt + cl_pt_no]);
			cl_pt_no++;
		}
		
		//cout << "no of center_line points included " << cl_pt_no <<" and total arc_length is " << temp_arclength << endl;

		double temp_T=0;
		double t_step = (traj[i].t1 - traj[i].t0)/cl_pt_no;
		//cout << "time_step = "<<t_step <<endl;
		double s[cl_pt_no], d[cl_pt_no], s_dot[cl_pt_no], d_dash[cl_pt_no], d_dot[cl_pt_no], s_dot_dot[cl_pt_no], d_dash_dash[cl_pt_no], d_dot_dot[cl_pt_no];
		double del_theta[cl_pt_no],final_kappa[cl_pt_no-1];
		double temp_a,temp_b;

		for (int k = 0; k < cl_pt_no; k++)
		{
			s[k] = traj[i].a_mat(0,0)*pow(temp_T,5) + traj[i].a_mat(1,0)*pow(temp_T,4) + traj[i].a_mat(2,0)*pow(temp_T,3) + traj[i].a_mat(3,0)*pow(temp_T,2) + 
					traj[i].a_mat(4,0)*temp_T + traj[i].a_mat(5,0); 					
			d[k] = traj[i].b_mat(0,0)*pow(s[k],5) + traj[i].b_mat(1,0)*pow(s[k],4) + traj[i].b_mat(2,0)*pow(s[k],3) + traj[i].b_mat(3,0)*pow(s[k],2) + 
					traj[i].b_mat(4,0)*s[k] + traj[i].b_mat(5,0); 					 		
			
			s_dot[k] = 5*traj[i].a_mat(0,0)*pow(temp_T,4) + 4*traj[i].a_mat(1,0)*pow(temp_T,3) + 3*traj[i].a_mat(2,0)*pow(temp_T,2) + 2*traj[i].a_mat(3,0)*temp_T + 
						traj[i].a_mat(4,0);
			d_dash[k] = 5*traj[i].b_mat(0,0)*pow(s[k],4) + 4*traj[i].b_mat(1,0)*pow(s[k],3) + 3*traj[i].b_mat(2,0)*pow(s[k],2) + 2*traj[i].b_mat(3,0)*s[k] + 
						traj[i].b_mat(4,0);
			
			d_dot[k] = s_dot[k]*d_dash[k];
			
			s_dot_dot[k] = 20*traj[i].a_mat(0,0)*pow(temp_T,3) + 12*traj[i].a_mat(1,0)*pow(temp_T,2) + 6*traj[i].a_mat(2,0)*temp_T + 2*traj[i].a_mat(3,0);
			d_dash_dash[k] = 20*traj[i].b_mat(0,0)*pow(s[k],3) + 12*traj[i].b_mat(1,0)*pow(s[k],2) + 6*traj[i].b_mat(2,0)*s[k] + 2*traj[i].b_mat(3,0);

			d_dot_dot[k] = d_dash_dash[k]*pow(s_dot[k],2) + d_dash[k]*s_dot_dot[k];			

			global_theta_x[k] = theta_r[prev_cl_pt+k] + atan(d_dash[k]/(1 - kappa[prev_cl_pt+k]*d[k]));					/////////////////// omega
			del_theta[k] = global_theta_x[k] - theta_r[prev_cl_pt+k];

			global_velocity[k] = (1 - kappa[prev_cl_pt+k]*d[k])*s_dot[k]/cos(del_theta[k]);				/////////////////// velocity

			//cout << "v = "<<global_velocity[k] << " theta = "<< global_theta_x[k] <<" and traj "<<i << " and point "<< k<<endl;

			if(k!=0)
			{		
				//////////omega calculation////////////////
				global_omega[k-1] = (global_theta_x[k] - global_theta_x[k-1])/t_step; 	

				////////////kappa_max calculation////////////				
				temp_a = (kappa[prev_cl_pt+k]*d[k] - kappa[prev_cl_pt+k-1]*d[k-1])/(s[k] - s[k-1]);
				temp_b = ((d_dash_dash[k] + temp_a*tan(del_theta[k])) * (pow(cos(del_theta[k]),2)/(1 - kappa[prev_cl_pt+k]*d[k]))) + kappa[prev_cl_pt+k];
				final_kappa[k] = temp_b * (cos(del_theta[k])/(1 - kappa[prev_cl_pt+k]*d[k]));
				
				//cout<< "final_kappa = "<<final_kappa[k] <<endl;

				if (std::abs(final_kappa[k]) > max_robot_curvature)
					traj[i].max_curvature_status = 1;
				else
					traj[i].max_curvature_status = 0;	
			
			}	

			if(traj[i].max_curvature_status == 1)
			{
				cout << "curvature exceeded for traj:" << i << " and kappa_max = "<< std::abs(final_kappa[k])<< endl;
				
				break;
			}


			temp_T = temp_T + t_step;			
		}

		if(traj[i].max_curvature_status == 0)
		{
			optimum_traj_no = i;
			cout << "optimum_traj is " <<optimum_traj_no <<" and its deviation is "<<traj[optimum_traj_no].c_b_mat(1,0) <<endl;

			//////////////storing velocity and theta values////////////
			// for (int l = 0; l < cl_pt_no; ++l)
			// {
			// 		myfile3 << global_velocity[l] <<" " << global_theta_x[l] << endl;
			// }

			break;
		}

		temp_T = 0;

	}	

	global_x[0] = robot_current_x; global_y[0] = robot_current_y;
	double t_step = (traj[optimum_traj_no].t1 - traj[optimum_traj_no].t0)/cl_pt_no;
	
	for (int m = 1; m < cl_pt_no; m++)
	{
		global_x[m] = global_x[m-1] + global_velocity[m-1]*cos(global_theta_x[m-1])*t_step;
		global_y[m] = global_y[m-1] + global_velocity[m-1]*sin(global_theta_x[m-1])*t_step;			
		myfile3 << global_x[m] <<" " << global_y[m] << endl;
	}

	myfile3.close();
}


void convert2worldxy()
{
	global_x[0] = global_y[0] = 0;
	double t_step = (traj[optimum_traj_no].t1 - traj[optimum_traj_no].t0)/cl_traj_pt;
	
	for (int m = 1; m < cl_traj_pt; ++m)
	{
		global_x[m] = global_x[m-1] + global_velocity[m-1]*cos(global_theta_x[m-1])*t_step;
		global_y[m] = global_y[m-1] + global_velocity[m-1]*sin(global_theta_x[m-1])*t_step;			
	}

}



int main(int argc, char **argv)
{
  
  	ros::init(argc, argv, "local_planner");

  	ros::NodeHandle n;

	ros::Subscriber odom_sub = n.subscribe("/odometry/filtered", 1, odom_callback);
	ros::Subscriber imu_sub = n.subscribe("/imu/data", 1, imu_callback);
	ros::Subscriber scan_sub = n.subscribe("/scan", 1, scan_callback );


  	velocity_pub = n.advertise<geometry_msgs::Twist>("/husky_velocity_controller/cmd_vel", 1000);
  	ros::Rate loop_rate(set_loop_rate);

 	///////////////////wait for initial data 10000000 loop
 
 	get_global_waypoints();
	get_center_line();
	
	for(int w=0; w<10000000; w++)
	{ros::spinOnce();}
	
	clock_t tStart = clock();
	get_current_statedata();
	get_traj();
	if(obs_no>=0)
	{	
		get_obstacles();
		check_collision();
	}
	compute_cost();
	check_curvature_convert2vw();
	int z=0;
	printf("Exec Time: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

  	while (ros::ok())
  	{
    	

  		if(z==30)
  		{	
  			//int y=0;
    		clock_t tStart = clock();
			get_current_statedata();
			//y++; ROS_INFO("y = %d",y);
			get_traj();
			//y++; ROS_INFO("y = %d",y);
			
			if(obs_no>=0)
			{	
				get_obstacles();
				//y++; ROS_INFO("y = %d",y);
				check_collision();
				//y++; ROS_INFO("y = %d",y);	
			}
			
			compute_cost();
			//y++; ROS_INFO("y = %d",y);
			check_curvature_convert2vw();
			//y++; ROS_INFO("y = %d",y);
			z=0;
			printf("Exec Time: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
		}

  		publish_vw(z);
  		z++;
  		//ROS_INFO("z = %d",z);

  		//if(!stop_replanning)
  			//ros::shutdown();
  		
    	ros::spinOnce();
    	loop_rate.sleep();
    }


  	return 0;

}
