////at the end create a function out of this : input is max_curvature, way_pts : output is way_traj 

#include <math.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <Eigen/Dense>

#define PI ((double)3.14159265)


#define radiusofcircle ((double)4)

#define cl_traj_pt 200


using Eigen::MatrixXd;
using Eigen::VectorXd;
using namespace Eigen;
using namespace std;



// double way_x[6]={ 0, 00, 10, 10, 40, 70 };
// double way_y[6]={ 0, 10, 10, 30, 30, 00 };


Vector2d cl_way_traj[cl_traj_pt];
double theta_r[(int)(cl_traj_pt)], arc_length[(int)(cl_traj_pt)], kappa[(int)(cl_traj_pt)];



int main(void)
{	


	ofstream myfile;
  	myfile.open ("example.txt"); 			

  	double tempx, tempy, theta;

  	theta = (double)PI/2;
  	cl_way_traj[0](0) = cl_way_traj[0](1)= 0;
  	myfile <<cl_way_traj[0](0)<<" "<<cl_way_traj[0](1)<< endl;

  	for(int i = 1; i<cl_traj_pt; i++)
  	{
  		theta = theta - 2*PI/cl_traj_pt;
  		//printf("%lf \n", theta);

  		cl_way_traj[i](0) = (double)radiusofcircle* cos(theta);
  		cl_way_traj[i](1) = (double)radiusofcircle* sin(theta) - radiusofcircle; 

  		myfile <<cl_way_traj[i](0)<<" "<<cl_way_traj[i](1)<< endl;
  	
  	}

  	//////////////////////calculation for theta and kappa (curvature)	

	theta_r[0] = atan2((cl_way_traj[1](1) - cl_way_traj[0](1)),(cl_way_traj[1](0) - cl_way_traj[0](0)));
	arc_length[0] = (cl_way_traj[1] - cl_way_traj[0]).norm();
	
	for(int j=1; j<cl_traj_pt-1; j++)
	{
		theta_r[j] = atan2((cl_way_traj[j+1](1) - cl_way_traj[j](1)),(cl_way_traj[j+1](0) - cl_way_traj[j](0)));
		arc_length[j] = arc_length[j-1] + (cl_way_traj[j+1] - cl_way_traj[j]).norm();
		kappa[j-1] = (theta_r[j] - theta_r[j-1])/(arc_length[j] - arc_length[j-1]);			
		
		 cout<<"theta_r(" <<(j) << ") = " << theta_r[j]<<endl;
		 cout<<"arc_length(" <<(j) << ") = " << arc_length[j]<<endl;
		 cout<<"kappa(" <<(j-1) << ") = " << kappa[j-1]<< "\n"<<endl; 
	
	}
	
	theta_r[(int)cl_traj_pt-1] = theta_r[(int)cl_traj_pt-2];
	arc_length[(int)cl_traj_pt-1] = arc_length[(int)cl_traj_pt-2];
	kappa[(int)cl_traj_pt-1] = kappa[(int)cl_traj_pt-2] = kappa[(int)cl_traj_pt-3];
	
	 cout<<"theta_r(" <<(int)cl_traj_pt-1 << ") = " << theta_r[(int)cl_traj_pt-1]<<endl;
	 cout<<"arc_length(" <<(int)cl_traj_pt-1 << ") = " << arc_length[(int)cl_traj_pt-1]<<endl;
	 cout<<"kappa(" <<(int)cl_traj_pt-1 << ") = " << kappa[(int)cl_traj_pt-1]<< "\n"<<endl; 			

	/////////////////////////////////////


	// Vector2d W1, W2, W3, U1, U2, Ud, B0, B1, B2, B3, E0, E1, E2, E3;	

	// double c1=7.2364, c2=(sqrt(6)-1)*2/5, c3, kmax=max_curvature;  
	// double beta, gammaa, dk, hb, he, gb, ge, kb, ke ;

	// int ways=way_pt_no,traj_pt=0;

	// c3 = (c2+4)/(c1+6);

	// int i=0;
	// double temp_slope1,temp_slope2;
	// double t=0;

	// way_traj[traj_pt] << 0,0;
	// Vector2d temp_pos = way_traj[traj_pt], temp_pos1;
	// traj_pt++;
		
	

	// while(i < (ways-2))
	// {	


	// 	// W[0][0]= way_x[i]; W[0][1]= way_y[i]; 
	// 	// W[1][0]= way_x[i+1]; W[1][1]= way_y[i+1];
	// 	// W[2][0]= way_x[i+2]; W[2][1]= way_y[i+2];	
		
	// 	// temp_slope1 = atan((way_pt[i+1](1)-way_pt[i](1))/(way_pt[i+1](0)-way_pt[i](0))); 	
	// 	// temp_slope2 = atan((way_pt[i+2](1)-way_pt[i+1](1))/(way_pt[i+2](0)-way_pt[i+1](0))); 	


	// 	W1 = way_pt[i];
	// 	W2 = way_pt[i+1];
	// 	W3 = way_pt[i+2];

	// 	// W1 = way_pt[1];
	// 	// W2 = way_pt[2];
	// 	// W3 = way_pt[3];
	// 	// //cout<<" W1 " << W1 <<" W2 "<<W2 <<" W3 "<< W3<<endl; 

	// 	///////////////old ////////////////////
	// 	// temp_slope1 = atan((W2(1) - W1(1))/(W2(0) - W1(0)));	
	// 	// temp_slope2 = atan((W3(1) - W2(1))/(W3(0) - W2(0)));
	// 	// //cout << temp_slope1 <<" "<< temp_slope1<<endl;	

	// 	// if (temp_slope1<0)
	// 	// 	temp_slope1=temp_slope1+PI;

	// 	// if (temp_slope2<0)
	// 	// 	temp_slope2=temp_slope2+PI;
	// 	// gammaa = fabs(temp_slope2 - temp_slope1);
	// 	// beta = gammaa/2;
	// 	////////////////////////////////////////////

	// 	Vector3d MW21, MW32, gamma_cross_vector;
	// 	MW21 << (W2(0)-W1(0)), (W2(1)-W1(1)), (0);
	// 	MW32 << (W3(0)-W2(0)), (W3(1)-W2(1)), (0);

	// 	gamma_cross_vector = MW21.cross(MW32);

	// 	gammaa = asin((gamma_cross_vector.norm())/(((W2-W1).norm())*(((W3-W2).norm()))));
		
	// 	beta = (PI-gammaa)/2;
	// 	dk = (pow(c2+4,2)*sin(beta))/(54*c3*kmax*pow(cos(beta),2));
	// 	hb=he=c3*dk; 
	// 	gb=ge=c2*c3*dk;
	// 	kb=ke=(6*c3*cos(beta)*dk)/(c2+4);	

	// 	//U1 = (W[1]-W[2])/(W[1].norm() * W[2].norm());
	// 	//U2 = (W[2]-W[3])/(W[2].norm() * W[3].norm());

	// 	U1 = (W1-W2)/((W1-W2).norm());
	// 	U2 = (W3-W2)/((W3-W2).norm());
		
	// 	B0 = W2 + dk*U1;
	// 	B1 = B0 - gb*U1;
	// 	B2 = B1 - hb*U1;
	
	// 	E0 = W2 + dk*U2;
	// 	E1 = E0 - ge*U2;
	// 	E2 = E1 - he*U2;

	// 	Ud = (E2-B2)/((E2-B2).norm());

	// 	B3 = B2 + kb*Ud;
	// 	E3 = E2 - ke*Ud;


	// 	// cout << "Here is the vector B0:\n" << B[0] << endl;	
	// 	// cout << "Here is the vector B1:\n" << B[1] << endl;	
	// 	// cout << "Here is the vector B2:\n" << B[2] << endl;



	// 	////interpolation code 

	// 	temp_pos1 = (pow(1-t,3)*B0 + 3*pow(1-t,2)*t*B1 + 3*(1-t)*pow(t,2)*B2 + pow(t,3)*B3);
	// 	//cout<<"temp_pos1 "<<temp_pos1<<endl;

	// 	//if(traj_pt==1)
	// 	{	
		
	// 		if(( temp_pos1 - temp_pos ).norm() > 0.2)
	// 		{
	// 			Vector2d temp_step = (temp_pos1 - temp_pos)/cl_traj_pt;
	// 			for(int x=0; x<cl_traj_pt; x++)
	// 			{
	// 				way_traj[traj_pt] = way_traj[traj_pt-1] + temp_step;
	// 				//cout <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl;
	// 				myfile <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl; 
	// 				traj_pt++;	
	// 			}

	// 		}
	// 	}
	// 	////////////////////
			
	// 	while(t<=1)
	// 	{	
	// 		way_traj[traj_pt] = (pow(1-t,3)*B0 + 3*pow(1-t,2)*t*B1 + 3*(1-t)*pow(t,2)*B2 + pow(t,3)*B3); 	
	// 		//cout <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl;
	// 		myfile <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl;			
	// 		traj_pt++;
	// 		t += center_line_dt*2;
	// 	}	
		
	// 	while(t>=0)
	// 	{	
	// 		way_traj[traj_pt] = (pow(1-t,3)*E0 + 3*pow(1-t,2)*t*E1 + 3*(1-t)*pow(t,2)*E2 + pow(t,3)*E3); 	
	// 		//cout <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl;
	// 		myfile <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl;
	// 		traj_pt++;
	// 		t -= center_line_dt*2;
	// 	}


	// 	temp_pos =  way_traj[traj_pt-1];
	// 	//cout << "temp_pos " <<temp_pos<<endl;
	// 	i++;
	// }


	// 	////interpolation code 

	// 	temp_pos1 = way_pt[way_pt_no-1];
	// 	//cout<<"temp_pos1 "<<temp_pos1<<endl;

	// 	//if(traj_pt==1)
	// 	{	
		
	// 		if(( temp_pos1 - temp_pos ).norm() > 0.2)
	// 		{
	// 			Vector2d temp_step = (temp_pos1 - temp_pos)/cl_traj_pt;
	// 			for(int x=0; x<cl_traj_pt; x++)
	// 			{
	// 				way_traj[traj_pt] = way_traj[traj_pt-1] + temp_step;
	// 				//cout <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl;
	// 				myfile <<way_traj[traj_pt](0)<<" "<<way_traj[traj_pt](1)<< endl; 
	// 				traj_pt++;	
	// 			}

	// 		}
	// 	}
	// 	////////////////////

	myfile.close();
	return 0;
}